<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03: ACTIVITY</title>
</head>
<body>
	<h1>Person</h1>
   <?php $person = new Person("Senku"," ", "Ishigami") ?>
   <p><?= $person->printName() ?></p>

   <h1>Developer</h1>
   <?php $developer = new Developer("John", "Finch", "Smith") ?>
   <p><?= $developer->printName() ?></p>

   <h1>Engineer</h1>
   <?php $engineer = new Engineer("Harold", "Myers", "Reese") ?>
   <p><?= $engineer->printName() ?></p>




</body>
</html>