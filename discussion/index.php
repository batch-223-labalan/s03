<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03: Objects from Variable</title>
</head>
<body>
	<h1>Objects from Variable</h1>
	<p><?php var_dump($buildingObject); ?></p>
	<p><?php echo $buildingObject ->name; ?></p>
	
	
	<h2>Objects from Classes</h2>
	<p><?php var_dump($building); ?></p>
	<p><?php echo $building ->name; ?></p>

	<p><?php echo $building ->printName(); ?></p>

	<h2>Inheritance (Condominium Object)</h2>
	<p><?php var_dump($condominium); ?></p>
	<p><?php echo $condominium ->printName() ?></p>


	
	<h2>Polymorphism</h2>
	<p><?php echo $condo ->printName() ?></p>

</body>
</html>