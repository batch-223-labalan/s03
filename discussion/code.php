<?php

// OBJECT
//Using procedural approach

$buildingObject = (object)[
'name' => 'Caswynn Building',
'floors' => 8,
'address' => 'Brgy. Sacred Heart, Quezon City, Philippines'
];


//Objects from Classes

class Building{
	public $name;
	public $floors;
	public $address;

	public function __construct($name, $floors, $address){
		$this ->name = $name;
		$this ->floors = $floors;
		$this -> address = $address;
	}
	//Method
	public function printName(){
		return "The name of the building is $this->name";
	}
}

	//Instantiate the Building class to create a new building object

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines' );


//INHERITANCE & POLYMORPHISM

class Condominium extends Building{

}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');


 //With polymorphism

class Condo extends Building{
	public function printName(){
		return "The name of the condominium is $this->name";
	}

}

$condo = new Condo('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');













